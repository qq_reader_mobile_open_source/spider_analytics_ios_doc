Spider
===

Demo 工程
---

首先克隆该仓库，然后在 Example 目录下运行 `pod install`，最后编译运行 Demo 工程。

安装
---

使用 Cocoapods 安装：

```ruby
pod 'Spider'
```

快速上手
---

```objective-c
- (BOOL)            application:(UIApplication *)application
  didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    …
    // 启动 Spider
    [Spider startWithConfigurator:^(id<SPDConfiguration> config) {
        config.delegate = self;
    }];
}
```

```objective-c
@interface MYAppDelegate (SPDDelegate) <SPDDelegate>

- (void)spd_receiveResult:(id)result
{
    // 上传统计结果到您的渠道
}

@end
```

附加属性绑定
---

```objective-c
- (UITableViewCell *)cellForRow:(NSIndexPath *)indexPath
                      tableView:(UITableView *)tableView {

    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"identifier"
                                                            forIndexPath:indexPath];
    cell.spd_attributes = @{@"did":@"123",@"dt":@"bid"};  // 在视图上绑定附加属性
}

```

通过向 `@property NSDictionary *spd_attributes;` 赋值，手动添加该 `UIResponder` 的附加属性。

赋值后，附加属性会随着该节点上发生的事件一起上报。如：

- `aView.spd_attributes = @{@"foo": @"bar"}`, `aView` 上发生曝光，`{"foo": "bar"}` 会随着事件一起上报。
- `aButton.spd_attributes = @{@"foo": @"bar"}`, `aButton` 上发生点击，`{"foo": "bar"}` 会随着事件一起上报。

附加属性会被子节点继承。如：

- `aViewController = @{@"foo": @"bar"}`, 任何 `aViewController` 的子视图上发生事件，`{"foo": "bar"}` 都会随着事件上报。


文档
---

- [API文档](https://qq_reader_mobile_open_source.gitlab.io/spider_analytics_ios_doc/)


插件
---

- [SpiderWeaver](./spider_weaver.md) 官方统计平台接入，及圈选器 UI


作者
---

QQ阅读终端研发团队，
qqreader_mobile_tech@yuewen.com
