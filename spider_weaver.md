# SpiderWeaver

[![CI Status](https://img.shields.io/travis/p_sykang/SpiderWeaver.svg?style=flat)](https://travis-ci.org/p_sykang/SpiderWeaver)
[![Version](https://img.shields.io/cocoapods/v/SpiderWeaver.svg?style=flat)](https://cocoapods.org/pods/SpiderWeaver)
[![License](https://img.shields.io/cocoapods/l/SpiderWeaver.svg?style=flat)](https://cocoapods.org/pods/SpiderWeaver)
[![Platform](https://img.shields.io/cocoapods/p/SpiderWeaver.svg?style=flat)](https://cocoapods.org/pods/SpiderWeaver)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## 依赖库

1、Spider

2、QRFoundation

3、QRNetWorker

4、SpiderWeaverUILib

## 安装

SpiderWeaver is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'SpiderWeaver'
```

## 接入方式

##### 初始化

```objective-c
#import <SpiderWeaver/SpiderWeaver.h>

[SpiderWeaver startWithConfiguration:(NSObject<SpiderWeaverConfiguration> *)customObject];
```

##### 显示与隐藏

```objective-c
//显示
[SpiderWeaver show];
//隐藏
[SpiderWeaver hide];
```

##### 扩展功能

1、支持显示动态绑定数据的悬浮窗功能（默认为开启状态），如需隐藏。请调用如下方法

```objective-c
//是否支持本地动态数据的提示悬浮窗
+(void)enableLocalDataTipShow:(BOOL)show;
```

## Tips

1、埋点数据平台目前为http接口，所以需要配置plist文件的ats设置为允许

## Author

p_sykang, kangsiyu@tencent.com

## License

SpiderWeaver is available under the MIT license. See the LICENSE file for more info.
